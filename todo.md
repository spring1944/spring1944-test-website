## TODO

## Videos
1 - Mettre sur peertube les vidéos -> les deux: celle du background et celle du trailer, ou seulement celle du trailer.
2 - Ajouter l'iframe de la ou les vidéos Peertube uploadées.
3 - Éviter de preloader le trailer
4 - Ajouter lightbox pour TOUTES les images du site principal

## Images
5 - Diviser le site en deux : gallerie et site principal
6 - Réduire les images de moitié (au moins) par compression
7 - Les placer sur un site plus rapide genre CDN ? Cloudflare par exemple.

## Nouveau thème
8 - Créer un nouveau thème comme explicité dans le carnet de route.

## Traduction
9 - Commencer à réfléchir à une meilleure solution que de la traduction statique

### Français
10 - Faire le index-fr.html en statique

### Allemand
11 - Demander à JAL(a), sinon le faire(b).

# Milestones

1,2,3,5,6,10,11a -> jusqu'au 11/08/2019
